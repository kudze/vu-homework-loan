package kk.skaiciuokle.models;

public class LoanPayment {
    private Date date;
    private double balance;
    private double installment;
    private double interestAmount;
    private double repaymentAmount;
    private double paidInterest;
    private double leftInterest;
    private double leftTotalAmount;

    public LoanPayment(Date date, double balance, double installment, double interestAmount, double repaymentAmount, double paidInterest, double leftInterest, double leftTotalAmount) {
        this.date = date;
        this.balance = balance;
        this.installment = installment;
        this.interestAmount = interestAmount;
        this.repaymentAmount = repaymentAmount;
        this.paidInterest = paidInterest;
        this.leftInterest = leftInterest;
        this.leftTotalAmount = leftTotalAmount;
    }

    public Date getDate() {
        return date;
    }

    public double getBalance() {
        return balance;
    }

    public String getBalanceRounded() {
        return String.format("%.2f", balance);
    }

    public double getInstallment() {
        return installment;
    }

    public String getInstallmentRounded() {
        return String.format("%.2f", installment);
    }

    public double getInterestAmount() {
        return interestAmount;
    }

    public String getInterestAmountRounded() {
        return String.format("%.2f", interestAmount);
    }

    public double getRepaymentAmount() {
        return repaymentAmount;
    }

    public String getRepaymentAmountRounded() {
        return String.format("%.2f", repaymentAmount);
    }

    public double getPaidInterest() { return paidInterest; }

    public String getPaidInterestRounded() { return String.format("%.2f", paidInterest); }

    public double getLeftInterest() {
        return leftInterest;
    }

    public String getLeftInterestRounded() {
        if(leftInterest > -0.01 && leftInterest < 0.01)
            return "0.00";

        return String.format("%.2f", leftInterest);
    }

    public double getLeftTotalAmount() {
        return leftTotalAmount;
    }

    public String getLeftTotalAmountRounded() {
        if(leftTotalAmount > -0.01 && leftTotalAmount < 0.01)
            return "0.00";

        return String.format("%.2f", leftTotalAmount);
    }
}
