package kk.skaiciuokle.models;

import java.util.ArrayList;

public class LinearLoan extends Loan {

    public LinearLoan() {
        super();
    }

    public LinearLoan(double amount, Date startDate, Date endDate, double interestRate) {
        super(amount, startDate, endDate, interestRate);
    }

    private double getMonthlyRepayment() {
        int months = this.getMonthCount();

        if(this.hasDefermentPeriod())
            months -= Date.calculateMonthCountBetweenDates(this.getDefermentStartDate(), this.getDefermentEndDate()) + 1;

        return this.getAmount() / months;
    }

    @Override
    public double getTotalAmount() {
        double sum = 0;

        double interestRate = (this.getInterestRate() / 100.0) / 12.0;
        double balance = this.getAmount();
        double repayment = this.getMonthlyRepayment();
        for(Date i = new Date(this.getStartDate()); i.isLessThan(this.getEndDate()); i.incrementMonth()) {
            double innerRepayment = repayment;
            double interestAmount = balance * interestRate;

            if (this.hasDefermentPeriod() && (this.getDefermentStartDate().isLessOrEqualTo(i)) && (!this.getDefermentEndDate().isLessThan(i))) {
                innerRepayment = 0;
            }

            double installment = innerRepayment + interestAmount;
            balance -= repayment;

            sum += installment;
        }

        return sum;
    }

    @Override
    public ArrayList<LoanPayment> getPayments() {
        ArrayList<LoanPayment> result = new ArrayList<>();

        double interestRate = (this.getInterestRate() / 100.0) / 12.0;
        double balance = this.getAmount();
        double repayment = this.getMonthlyRepayment();
        double paidInterest = 0;
        double totalInterest = this.getTotalInterest();
        double totalAmount = this.getTotalAmount();
        for(Date i = new Date(this.getStartDate()); i.isLessThan(this.getEndDate()); i.incrementMonth())
        {
            double innerRepayment = repayment;
            double interestAmount = balance * interestRate;

            if(this.hasDefermentPeriod() && (this.getDefermentStartDate().isLessOrEqualTo(i)) && (!this.getDefermentEndDate().isLessThan(i)))
            {
                innerRepayment = 0;
            }

            double installment = innerRepayment + interestAmount;

            paidInterest += interestAmount;
            double leftInterest = totalInterest - paidInterest;
            totalAmount -= installment;

            result.add(new LoanPayment(new Date(i), balance, installment, interestAmount, innerRepayment, paidInterest, leftInterest, totalAmount));

            balance -= innerRepayment;
        }

        return result;
    }
}
