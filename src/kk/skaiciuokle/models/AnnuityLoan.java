package kk.skaiciuokle.models;

import java.util.ArrayList;

public class AnnuityLoan extends Loan {

    public AnnuityLoan()
    {
        super();
    }

    public AnnuityLoan(float amount, Date startDate, Date endDate, double interestRate)
    {
        super(amount, startDate, endDate, interestRate);
    }

    public double getTotalAmount() {
        if(!this.hasDefermentPeriod())
            return this.getInstallment() * this.getMonthCount();

        else {
            double interestRate = (this.getInterestRate() / 100.0) / 12.0;
            double installment = this.getInstallment();
            double balance = this.getAmount();
            double sum = 0;

            for(Date i = new Date(this.getStartDate()); i.isLessThan(this.getEndDate()); i.incrementMonth())
            {
                double innerInstallment = installment;
                double interestAmount = balance * interestRate;

                if(this.hasDefermentPeriod() && (this.getDefermentStartDate().isLessOrEqualTo(i)) && (!this.getDefermentEndDate().isLessThan(i)))
                {
                    innerInstallment = interestAmount;
                }

                sum += innerInstallment;
            }

            return sum;
        }
    }

    public double getInstallment() {
        return this.getAmount() / this.getAnnuityFactor();
    }

    public ArrayList<LoanPayment> getPayments() {
        ArrayList<LoanPayment> result = new ArrayList<>();

        double interestRate = (this.getInterestRate() / 100.0) / 12.0;
        double installment = this.getInstallment();
        double balance = this.getAmount();
        double paidInterest = 0;
        double totalInterest = this.getTotalInterest();
        double totalAmount = this.getTotalAmount();
        for(Date i = new Date(this.getStartDate()); i.isLessThan(this.getEndDate()); i.incrementMonth())
        {
            double innerInstallment = installment;
            double interestAmount = balance * interestRate;

            if(this.hasDefermentPeriod() && (this.getDefermentStartDate().isLessOrEqualTo(i)) && (!this.getDefermentEndDate().isLessThan(i)))
            {
                innerInstallment = interestAmount;
            }

            double repayAmount = innerInstallment - interestAmount;

            paidInterest += interestAmount;
            double leftInterest = totalInterest - paidInterest;
            totalAmount -= innerInstallment;

            result.add(new LoanPayment(new Date(i), balance, innerInstallment, interestAmount, repayAmount, paidInterest, leftInterest, totalAmount));

            balance -= repayAmount;
        }

        return result;
    }

    private double getAnnuityFactor() {
        int months = this.getMonthCount();

        if(this.hasDefermentPeriod())
            months -= Date.calculateMonthCountBetweenDates(this.getDefermentStartDate(), this.getDefermentEndDate()) + 1;

        double interestRate = (this.getInterestRate() / 100) / 12;
        return ((1 - Math.pow(1 + interestRate, -months)) / interestRate);
    }

}
