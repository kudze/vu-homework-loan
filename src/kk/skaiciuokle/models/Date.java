package kk.skaiciuokle.models;

import java.util.Calendar;

public class Date {
    public static int calculateMonthCountBetweenDates(Date startDate, Date endDate)
    {
        int year = endDate.getYear() - startDate.getYear();
        int month = endDate.getMonth() - startDate.getMonth();

        if (month < 1) {
            year--;
            month += 12;
        }

        month += year * 12;

        return month;
    }

    private int year;
    private int month;

    public Date() {
        this(Calendar.getInstance().getTime().getYear() + 1900, Calendar.getInstance().getTime().getMonth());
    }

    public Date(Date date) {
        this(date.year, date.month);
    }

    public Date(String str) throws Exception {
        if(!str.contains("-"))
            throw new Exception();

        String[] strs = str.split("-", 2);

        if(strs.length != 2)
            throw new Exception();

        try {
            this.year = Integer.parseInt(strs[0]);
            this.month = Integer.parseInt(strs[1]);
        } catch(NumberFormatException exception) {
            throw new Exception();
        }

        if(this.year < 1 || this.month < 1 || this.month > 12)
            throw new Exception();
    }

    public Date(int year, int month) {
        this.year = year;
        this.month = month;
    }

    public void incrementMonth() {
        if(this.month == 12) {
            this.year++;
            this.month = 1;

            return;
        }

        this.month++;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String toString() {
        return String.format("%04d", this.year) + "-" + String.format("%02d", this.month);
    }

    public boolean isLessThan(Date date) {
        if(this.getYear() < date.getYear())
            return true;

        if(this.getYear() > date.getYear())
            return false;

        return this.getMonth() < date.getMonth();
    }

    public boolean isLessOrEqualTo(Date date) {
        if(this.getYear() < date.getYear())
            return true;

        if(this.getYear() > date.getYear())
            return false;

        return this.getMonth() <= date.getMonth();
    }

    public boolean isEqualTo(Date date) {
        return this.year == date.year && this.month == date.month;
    }
}
