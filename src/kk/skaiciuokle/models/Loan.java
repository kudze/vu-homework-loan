package kk.skaiciuokle.models;

import java.util.ArrayList;

public abstract class Loan {
    private double amount;
    private Date startDate;
    private Date endDate;
    private double interestRate;
    private Date defermentStartDate = null;
    private Date defermentEndDate = null;

    public Loan() {
        this(12000, new Date(2020, 10), new Date(2029, 10), 2.5);
    }

    public Loan(double amount, Date startDate, Date endDate, double interestRate) {
        this.amount = amount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.interestRate = interestRate;
    }

    public double getAmount() {
        return amount;
    }

    public int getMonthCount() {
        return Date.calculateMonthCountBetweenDates(this.startDate, this.endDate);
    }

    public abstract double getTotalAmount();
    public abstract ArrayList<LoanPayment> getPayments();

    public ArrayList<LoanPayment> getFilteredPayments(Date startDate, Date endDate) {
        ArrayList<LoanPayment> result = new ArrayList<>();
        ArrayList<LoanPayment> input = this.getPayments();

        for (LoanPayment payment : input) {
            if((!payment.getDate().isLessThan(startDate)) && (payment.getDate().isLessOrEqualTo(endDate)))
                result.add(payment);
        }

        return result;
    }

    public double getTotalInterest() {
        return this.getTotalAmount() - this.getAmount();
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public Date getDefermentStartDate() {
        return defermentStartDate;
    }

    public Date getDefermentEndDate() {
        return defermentEndDate;
    }

    public void setDefermentPeriod(Date startDate, Date endDate)
    {
        this.defermentStartDate = startDate;
        this.defermentEndDate = endDate;
    }

    public void clearDefermentPeriod()
    {
        this.setDefermentPeriod(null, null);
    }

    public boolean hasDefermentPeriod() {
        return this.defermentStartDate != null;
    }
}
