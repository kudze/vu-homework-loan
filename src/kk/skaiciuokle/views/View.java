package kk.skaiciuokle.views;

import javafx.scene.Parent;
import javafx.scene.Scene;
import kk.skaiciuokle.Skaiciuokle;

public abstract class View {
    private Skaiciuokle skaiciuokle;
    private String styleSheet;

    public View(Skaiciuokle skaiciuokle)
    {
        this(skaiciuokle, null);
    }

    public View(Skaiciuokle skaiciuokle, String styleSheet)
    {
        this.skaiciuokle = skaiciuokle;
        this.styleSheet = styleSheet;
    }

    protected abstract Parent init();

    public Scene buildScene() {
        Scene scene = new Scene(this.init(), 1280, 720);

        if(this.styleSheet != null)
            scene.getStylesheets().add(this.styleSheet);

        return scene;
    }

    public Skaiciuokle getSkaiciuokle() {
        return skaiciuokle;
    }

    public String getStyleSheet() {
        return styleSheet;
    }

    public void setStyleSheet(String styleSheet) {
        this.styleSheet = styleSheet;
    }
}
