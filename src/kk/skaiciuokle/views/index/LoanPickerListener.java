package kk.skaiciuokle.views.index;

import kk.skaiciuokle.models.Loan;

public interface LoanPickerListener {

    void onLoanUpdate(Loan loan);

}
