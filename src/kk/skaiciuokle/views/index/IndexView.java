package kk.skaiciuokle.views.index;

import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import kk.skaiciuokle.Skaiciuokle;
import kk.skaiciuokle.models.Loan;
import kk.skaiciuokle.views.View;

public class IndexView extends View implements LoanPickerListener {
    private LoanPickerPanel loanPicker;
    private LoanTablePanel loanTable;
    private InstallmentGraphPanel installmentGraph;
    private LeftToPayGraphPanel leftToPayGraph;

    public IndexView(Skaiciuokle skaiciuokle) {
        super(skaiciuokle);

        this.setStyleSheet(getClass().getResource("style.css").toExternalForm());

        this.loanPicker = new LoanPickerPanel(this);
        this.loanTable = new LoanTablePanel(this.loanPicker.getLoan());
        this.installmentGraph = new InstallmentGraphPanel(this.loanPicker.getLoan());
        this.leftToPayGraph = new LeftToPayGraphPanel(this.loanPicker.getLoan());
    }

    @Override
    protected Parent init() {
        Parent dataInputBox = loanPicker;
        Parent loanTableBox = loanTable;

        VBox leftVerticalBox = new VBox(25, dataInputBox, installmentGraph, leftToPayGraph);
        HBox horizontalBox = new HBox(50, leftVerticalBox, loanTableBox);
        horizontalBox.setAlignment(Pos.CENTER);

        return horizontalBox;
    }

    @Override
    public void onLoanUpdate(Loan loan) {
        this.loanTable.update(loan);
        this.installmentGraph.update(loan);
        this.leftToPayGraph.update(loan);
    }
}
