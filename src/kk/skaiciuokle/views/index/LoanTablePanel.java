package kk.skaiciuokle.views.index;

import com.opencsv.CSVWriter;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import kk.skaiciuokle.models.Date;
import kk.skaiciuokle.models.Loan;
import kk.skaiciuokle.models.LoanPayment;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class LoanTablePanel extends VBox {
    private Loan loan;
    private Date filterStartDate;
    private Date filterEndDate;
    private Stage filterPopup;

    public LoanTablePanel(Loan loan) {
        this.update(loan);
    }

    public void update(Loan loan) {
        this.loan = loan;

        this.getChildren().clear();
        this.getChildren().add(this.constructTopBar());
        this.getChildren().add(this.constructTable());
        this.getChildren().add(this.constructSaveButton());
    }

    private Parent constructTopBar() {
        Text loanAmtText = new Text("Paskolos suma: " + String.format("%.2f", this.loan.getAmount()));
        Text loanTotalPayText = new Text("Iš viso sumokėti: " + String.format("%.2f", this.loan.getTotalAmount()));

        return new HBox(
                25,
                new VBox(
                        10,
                        loanAmtText,
                        loanTotalPayText
                ),
                this.constructFilterSection()
        );
    }

    private Parent constructFilterSection() {
        Text filterText = new Text("Filtras: " + this.constructFilterStatus());
        Button filterBtn = new Button("Nustatyti filtrą");
        filterBtn.setOnAction(evt -> this.startFilterPopup());

        return new VBox(
                10,
                filterText,
                filterBtn
        );
    }

    private void startFilterPopup() {
        if (this.filterPopup != null)
            this.stopFilterPopup();

        Scene filterScene = new Scene(this.constructFilterPopupContent(), 300, 200);
        this.filterPopup = new Stage();
        this.filterPopup.setTitle("Filtro nustatymai");
        this.filterPopup.setScene(filterScene);

        this.filterPopup.show();
    }

    private void setFilter(
            TextField filterStartDateField,
            TextField filterEndDateField,
            Text filterStartDateError,
            Text filterEndDateError
    ) {
        Date startDate = null;
        Date endDate = null;
        boolean success = true;

        try {
            startDate = new Date(filterStartDateField.getText());
        } catch (Exception exception) {
            filterStartDateField.getStyleClass().add("error");
            filterStartDateError.setText("Laukelio reikšmė turėtų būti data! (YYYY-MM)");
            success = false;
        }

        try {
            endDate = new Date(filterEndDateField.getText());
        } catch (Exception exception) {
            filterEndDateField.getStyleClass().add("error");
            filterEndDateError.setText("Laukelio reikšmė turėtų būti data! (YYYY-MM)");
            success = false;
        }

        if(success) {
            if(!startDate.isLessOrEqualTo(endDate))
            {
                filterStartDateField.getStyleClass().add("error");
                filterStartDateError.setText("Paskolos termino pradžia turėtų būti\nmažesnė už paskolos termino pabaigą");
            }

            else {
                this.filterStartDate = startDate;
                this.filterEndDate = endDate;
                this.stopFilterPopup();
                this.update(this.loan);
            }
        }
    }

    private void resetFilter() {
        this.stopFilterPopup();
        this.filterStartDate = null;
        this.filterEndDate = null;
        this.update(this.loan);
    }

    private Parent constructFilterPopupContent() {
        Label filterStartDateLabel = new Label("Filtro termino pradžia:");
        Label filterEndDateLabel = new Label("Filtro termino pabaiga:");
        TextField filterStartDateField = new TextField();
        TextField filterEndDateField = new TextField();
        Text filterStartDateError = new Text();
        Text filterEndDateError = new Text();
        filterStartDateError.setFill(Color.RED);
        filterEndDateError.setFill(Color.RED);

        if(this.filterStartDate != null)
        {
            filterStartDateField.setText(this.filterStartDate.toString());
            filterEndDateField.setText(this.filterEndDate.toString());
        }

        Button resetFilterBtn = new Button("Išimti filtrą");
        Button setFilterBtn = new Button("Nustatyti filtrą");

        resetFilterBtn.setOnAction(
                evt -> this.resetFilter()
        );

        setFilterBtn.setOnAction(
                evt -> this.setFilter(
                        filterStartDateField,
                        filterEndDateField,
                        filterStartDateError,
                        filterEndDateError
                )
        );

        return new VBox(10,
                new VBox(
                        10,
                        new VBox(
                                filterStartDateLabel,
                                filterStartDateField,
                                filterStartDateError
                        ),
                        new VBox(
                                filterEndDateLabel,
                                filterEndDateField,
                                filterEndDateError
                        )
                ),
                new HBox(
                        10,
                        setFilterBtn,
                        resetFilterBtn
                )
        );
    }

    private void stopFilterPopup() {
        if (this.filterPopup == null)
            return;

        this.filterPopup.close();
        this.filterPopup = null;
    }

    private String constructFilterStatus() {
        if (this.filterStartDate == null)
            return "Nėra";

        return "nuo " + this.filterStartDate.toString() + " iki " + this.filterEndDate;
    }

    private ArrayList<LoanPayment> getActiveLoanPayments() {
        if(this.filterStartDate == null)
            return this.loan.getPayments();

        return this.loan.getFilteredPayments(this.filterStartDate, this.filterEndDate);
    }

    private Parent constructTable() {
        TableView tableView = new TableView();

        TableColumn<LoanPayment, String> column1 = new TableColumn<>("Data");
        column1.setCellValueFactory(new PropertyValueFactory<>("date"));

        TableColumn<LoanPayment, String> column2 = new TableColumn<>("Balansas");
        column2.setCellValueFactory(new PropertyValueFactory<>("balanceRounded"));

        TableColumn<LoanPayment, String> column3 = new TableColumn<>("Installment");
        column3.setCellValueFactory(new PropertyValueFactory<>("installmentRounded"));

        TableColumn<LoanPayment, String> column4 = new TableColumn<>("Palūkanos");
        column4.setCellValueFactory(new PropertyValueFactory<>("interestAmountRounded"));

        TableColumn<LoanPayment, String> column5 = new TableColumn<>("Mokėti");
        column5.setCellValueFactory(new PropertyValueFactory<>("repaymentAmountRounded"));

        TableColumn<LoanPayment, String> column6 = new TableColumn<>("Sumokėtos palūkanos");
        column6.setCellValueFactory(new PropertyValueFactory<>("paidInterestRounded"));

        TableColumn<LoanPayment, String> column7 = new TableColumn<>("Liko palūkanų");
        column7.setCellValueFactory(new PropertyValueFactory<>("leftInterestRounded"));

        TableColumn<LoanPayment, String> column8 = new TableColumn<>("Liko viso");
        column8.setCellValueFactory(new PropertyValueFactory<>("leftTotalAmountRounded"));

        tableView.getColumns().addAll(column1, column2, column3, column4, column5, column6, column7, column8);
        tableView.getItems().addAll(this.getActiveLoanPayments());

        return tableView;
    }

    private Parent constructSaveButton() {
        Button button = new Button("Išsaugoti į CSV");
        button.setOnAction(evt -> this.saveToCSV());

        return button;
    }

    private void saveToCSV() {
        try {
            Writer writer = Files.newBufferedWriter(Paths.get("./ataskaita.csv"));

            CSVWriter csvWriter = new CSVWriter(
                    writer,
                    ';',
                    CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END
            );

            csvWriter.writeNext(new String[]{"Data", "Balansas", "Installment", "Palukanos", "Moketi", "Sumoketos palukanos", "Liko palukanu", "Liko viso"});

            for (LoanPayment payment : this.getActiveLoanPayments()) {
                csvWriter.writeNext(new String[]{
                        payment.getDate().toString(),
                        payment.getBalanceRounded(),
                        payment.getInstallmentRounded(),
                        payment.getInterestAmountRounded(),
                        payment.getRepaymentAmountRounded(),
                        payment.getPaidInterestRounded(),
                        payment.getLeftInterestRounded(),
                        payment.getLeftTotalAmountRounded()
                });
            }

            csvWriter.close();
            writer.close();

            constructSaveButton().getStyleClass().remove("error");
        } catch (IOException exception) {
            constructSaveButton().getStyleClass().add("error");
        }
    }
}
