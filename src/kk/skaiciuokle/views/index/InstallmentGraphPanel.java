package kk.skaiciuokle.views.index;

import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.VBox;
import javafx.scene.text.*;
import kk.skaiciuokle.models.Loan;
import kk.skaiciuokle.models.LoanPayment;

public class InstallmentGraphPanel extends VBox {
    private Loan loan;

    public InstallmentGraphPanel(Loan loan) {
        this.setAlignment(Pos.CENTER);

        this.update(loan);
    }

    public void update(Loan loan) {
        this.loan = loan;

        this.getChildren().clear();
        this.getChildren().add(this.constructGraph());
    }

    private Parent constructGraph() {
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();

        xAxis.setLabel("Mėnesis");
        yAxis.setLabel("Pinigai");

        LineChart<String, Number> chart = new LineChart<String, Number>(xAxis, yAxis);
        chart.setTitle("Installment graph");
        chart.setCreateSymbols(false);

        XYChart.Series<String, Number> installmentSeries = new XYChart.Series<String, Number>();
        installmentSeries.setName("Installment");

        XYChart.Series<String, Number> paySeries = new XYChart.Series<String, Number>();
        paySeries.setName("Moketi");

        XYChart.Series<String, Number> interestSeries = new XYChart.Series<String, Number>();
        interestSeries.setName("Palukanos");

        boolean first = true;
        double minGraphValue = 0;
        double maxGraphValue = 0;
        for(LoanPayment payment : loan.getPayments())
        {
            if(first) {
                minGraphValue = payment.getInstallment();
                minGraphValue = Math.min(minGraphValue, payment.getInterestAmount());
                minGraphValue = Math.min(minGraphValue, payment.getRepaymentAmount());

                maxGraphValue = payment.getInstallment();
                maxGraphValue = Math.max(maxGraphValue, payment.getInterestAmount());
                maxGraphValue = Math.max(maxGraphValue, payment.getRepaymentAmount());

                first = false;
            } else {
                minGraphValue = Math.min(minGraphValue, payment.getInstallment());
                minGraphValue = Math.min(minGraphValue, payment.getInterestAmount());
                minGraphValue = Math.min(minGraphValue, payment.getRepaymentAmount());

                maxGraphValue = Math.max(maxGraphValue, payment.getInstallment());
                maxGraphValue = Math.max(maxGraphValue, payment.getInterestAmount());
                maxGraphValue = Math.max(maxGraphValue, payment.getRepaymentAmount());
            }

            installmentSeries.getData().add(new XYChart.Data<>(payment.getDate().toString(), payment.getInstallment()));
            paySeries.getData().add(new XYChart.Data<>(payment.getDate().toString(), payment.getRepaymentAmount()));
            interestSeries.getData().add(new XYChart.Data<>(payment.getDate().toString(), payment.getInterestAmount()));
        }

        chart.getData().add(installmentSeries);
        chart.getData().add(paySeries);
        chart.getData().add(interestSeries);
        yAxis.setAutoRanging(false);
        yAxis.setLowerBound(Math.max(0, Math.round(minGraphValue - 5)));
        yAxis.setUpperBound(Math.round(maxGraphValue + 5));
        yAxis.setTickUnit(25);
        yAxis.setMinorTickVisible(false);

        return chart;
    }
}
