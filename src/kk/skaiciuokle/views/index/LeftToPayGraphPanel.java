package kk.skaiciuokle.views.index;

import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.VBox;
import kk.skaiciuokle.models.Loan;
import kk.skaiciuokle.models.LoanPayment;

public class LeftToPayGraphPanel extends VBox {
    private Loan loan;

    public LeftToPayGraphPanel(Loan loan) {
        this.setAlignment(Pos.CENTER);

        this.update(loan);
    }

    public void update(Loan loan) {
        this.loan = loan;

        this.getChildren().clear();
        this.getChildren().add(this.constructGraph());
    }

    private Parent constructGraph() {
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();

        xAxis.setLabel("Mėnesis");
        yAxis.setLabel("Pinigai");

        LineChart<String, Number> chart = new LineChart<String, Number>(xAxis, yAxis);
        chart.setTitle("Mokejimo grafas");
        chart.setCreateSymbols(false);

        XYChart.Series<String, Number> leftToPay = new XYChart.Series<String, Number>();
        leftToPay.setName("Liko viso");

        for(LoanPayment payment : loan.getPayments())
        {
            leftToPay.getData().add(new XYChart.Data<>(payment.getDate().toString(), payment.getLeftTotalAmount()));
        }

        chart.getData().add(leftToPay);
        return chart;
    }
}
