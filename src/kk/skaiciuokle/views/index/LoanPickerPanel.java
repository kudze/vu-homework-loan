package kk.skaiciuokle.views.index;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import kk.skaiciuokle.models.AnnuityLoan;
import kk.skaiciuokle.models.Date;
import kk.skaiciuokle.models.LinearLoan;
import kk.skaiciuokle.models.Loan;

public class LoanPickerPanel extends VBox {
    private Loan loan = new AnnuityLoan();
    private LoanPickerListener listener;

    private Date defermentStartDate;
    private Date defermentEndDate;
    private Stage defermentPopup;

    //Updated texts.
    private Text deferementPeriodText = new Text("Mokėjimų atostogos: Nėra");

    //Data entry.
    private TextField loanAmountField = new TextField();
    private TextField interestField = new TextField();
    private TextField loanStartDateField = new TextField();
    private TextField loanEndDateField = new TextField();
    private RadioButton linearGraphRadio = new RadioButton("Linijinis");
    private RadioButton annuityGraphRadio = new RadioButton("Anuiteto");

    //Errors
    private Text loanAmountError = new Text();
    private Text interestError = new Text();
    private Text loanStartDateError = new Text();
    private Text loanEndDateError = new Text();

    public LoanPickerPanel(LoanPickerListener listener) {
        super(10);

        this.listener = listener;
        Label loanAmountLabel = new Label("Paskolos suma:");
        Label interestLabel = new Label("Palūkanos (Metinis procentas):");
        Label loanStartDateLabel = new Label("Termino pradžia:");
        Label loanEndDateLabel = new Label("Termino pabaiga:");

        loanAmountError.setFill(Color.RED);
        interestError.setFill(Color.RED);
        loanStartDateError.setFill(Color.RED);
        loanEndDateError.setFill(Color.RED);

        ToggleGroup toggleGroup = new ToggleGroup();
        linearGraphRadio.setToggleGroup(toggleGroup);
        annuityGraphRadio.setToggleGroup(toggleGroup);

        Button calculateButton = new Button("Atnaujinti");
        calculateButton.setOnAction(evt -> this.update());

        Button resetButton = new Button("Atstatyti");
        resetButton.setOnAction(evt -> this.reset());

        this.updateDefermentText();
        Button defermentButton = new Button("Nustatyti mokėjimų atostogas");
        defermentButton.setOnAction(evt -> this.startDefermentPopup());

        this.getChildren().addAll(
                new HBox(
                        10,
                        new VBox(
                                loanAmountLabel,
                                loanAmountField,
                                loanAmountError
                        ),
                        new VBox(
                                interestLabel,
                                interestField,
                                interestError
                        )
                ),
                new HBox(
                        10,
                        new VBox(
                                loanStartDateLabel,
                                loanStartDateField,
                                loanStartDateError
                        ),
                        new VBox(
                                loanEndDateLabel,
                                loanEndDateField,
                                loanEndDateError
                        )
                ),
                new HBox(
                        10,
                        linearGraphRadio,
                        annuityGraphRadio
                ),
                new VBox(
                        10,
                        deferementPeriodText,
                        defermentButton
                ),
                new HBox(
                        10,
                        calculateButton,
                        resetButton
                )
        );

        this.reset();
    }

    public void clearErrors() {
        this.loanAmountError.getStyleClass().remove("error");
        this.interestField.getStyleClass().remove("error");
        this.loanStartDateField.getStyleClass().remove("error");
        this.loanEndDateField.getStyleClass().remove("error");

        this.loanAmountError.setText("");
        this.interestError.setText("");
        this.loanStartDateError.setText("");
        this.loanEndDateError.setText("");
    }

    /**
     * Updates current loan to match data entry fields
     */
    public void update() {
        this.clearErrors();

        boolean success = true;
        Loan newLoan = null;

        if (this.linearGraphRadio.isSelected())
            newLoan = new LinearLoan();

        else if (this.annuityGraphRadio.isSelected())
            newLoan = new AnnuityLoan();

        else {
            this.annuityGraphRadio.getStyleClass().add("error");
            this.linearGraphRadio.getStyleClass().add("error");
            return;
        }

        try {
            newLoan.setAmount(Double.parseDouble(this.loanAmountField.getText()));
        } catch (NumberFormatException exception) {
            this.loanAmountField.getStyleClass().add("error");
            this.loanAmountError.setText("Laukelio reikšmė turėtų būti skaičius!");
            success = false;
        }

        try {
            newLoan.setInterestRate(Double.parseDouble(this.interestField.getText()));
        } catch (NumberFormatException exception) {
            this.interestField.getStyleClass().add("error");
            this.interestError.setText("Laukelio reikšmė turėtų būti skaičius!");
            success = false;
        }

        try {
            newLoan.setStartDate(new Date(this.loanStartDateField.getText()));
        } catch (Exception exception) {
            this.loanStartDateField.getStyleClass().add("error");
            this.loanStartDateError.setText("Laukelio reikšmė turėtų būti data! (YYYY-MM)");
            success = false;
        }

        try {
            newLoan.setEndDate(new Date(this.loanEndDateField.getText()));
        } catch (Exception exception) {
            this.loanEndDateField.getStyleClass().add("error");
            this.loanEndDateError.setText("Laukelio reikšmė turėtų būti data! (YYYY-MM)");
            success = false;
        }

        if(success) {
            if(!newLoan.getStartDate().isLessThan(newLoan.getEndDate()))
            {
                this.loanStartDateField.getStyleClass().add("error");
                this.loanStartDateError.setText("Paskolos termino pradžia turėtų būti\nmažesnė už paskolos termino pabaigą");
                success = false;
            }
        }

        if (success && this.defermentStartDate != null)
        {
            newLoan.setDefermentPeriod(this.defermentStartDate, this.defermentEndDate);
        }

        if (success) {
            this.loan = newLoan;
            this.listener.onLoanUpdate(this.getLoan());
        }
    }

    /**
     * Resets data entry fields to match current loan.
     */
    public void reset() {
        this.clearErrors();

        this.loanAmountField.setText(String.valueOf(this.loan.getAmount()));
        this.interestField.setText(String.valueOf(this.loan.getInterestRate()));
        this.loanStartDateField.setText(this.loan.getStartDate().toString());
        this.loanEndDateField.setText(this.loan.getEndDate().toString());

        if(this.loan instanceof AnnuityLoan) {
            linearGraphRadio.setSelected(false);
            annuityGraphRadio.setSelected(true);
        } else {
            linearGraphRadio.setSelected(true);
            annuityGraphRadio.setSelected(false);
        }

        if(!this.loan.hasDefermentPeriod())
        {
            this.defermentStartDate = null;
            this.defermentEndDate = null;
        } else {
            this.defermentStartDate = this.loan.getDefermentStartDate();
            this.defermentEndDate = this.loan.getDefermentEndDate();
        }

        this.updateDefermentText();
    }

    private void startDefermentPopup() {
        if (this.defermentPopup != null)
            this.stopDefermentPopup();

        Scene defermentScene = new Scene(this.constructDefermentPopupContent(), 300, 200);
        this.defermentPopup = new Stage();
        this.defermentPopup.setTitle("Mokėjimų atostogų nustatymai");
        this.defermentPopup.setScene(defermentScene);

        this.defermentPopup.show();
    }

    private void setDeferment(
            TextField defermentStartDateField,
            TextField defermentEndDateField,
            Text defermentStartDateError,
            Text defermentEndDateError
    ) {
        Date startDate = null;
        Date endDate = null;
        boolean success = true;

        try {
            startDate = new Date(defermentStartDateField.getText());
        } catch (Exception exception) {
            defermentStartDateField.getStyleClass().add("error");
            defermentStartDateError.setText("Laukelio reikšmė turėtų būti data! (YYYY-MM)");
            success = false;
        }

        try {
            endDate = new Date(defermentEndDateField.getText());
        } catch (Exception exception) {
            defermentEndDateField.getStyleClass().add("error");
            defermentEndDateError.setText("Laukelio reikšmė turėtų būti data! (YYYY-MM)");
            success = false;
        }

        if(success) {
            if(!startDate.isLessOrEqualTo(endDate))
            {
                defermentStartDateField.getStyleClass().add("error");
                defermentStartDateError.setText("Termino pradžia turėtų būti\nmažesnė už paskolos termino pabaigą");
            }

            else if(startDate.isLessThan(this.loan.getStartDate()))
            {
                defermentStartDateField.getStyleClass().add("error");
                defermentStartDateError.setText("Termino pradžia turėtų būti\nnemažesnė nei paskolos termino pradžia");
            }

            else if(!endDate.isLessOrEqualTo(this.loan.getEndDate()))
            {
                defermentEndDateField.getStyleClass().add("error");
                defermentEndDateError.setText("Termino pabaiga turėtų būti\nnedidesnė nei paskolos termino pabaiga");
            }

            else if(startDate.isEqualTo(this.loan.getStartDate()) && endDate.isEqualTo(this.loan.getEndDate()))
            {
                defermentStartDateField.getStyleClass().add("error");
                defermentStartDateError.setText("Paskola turi turėti bent 1 neatostoginį mokėjimų mėnesį!");
            }

            else {
                this.defermentStartDate = startDate;
                this.defermentEndDate = endDate;
                this.stopDefermentPopup();
                this.updateDefermentText();
            }
        }
    }

    private void resetDeferment() {
        this.stopDefermentPopup();
        this.defermentStartDate = null;
        this.defermentEndDate = null;
        this.updateDefermentText();
    }

    private Parent constructDefermentPopupContent() {
        Label defermentStartDateLabel = new Label("Mokėjimo atostogų termino pradžia:");
        Label defermentEndDateLabel = new Label("Mokėjimo atostogų pabaiga:");
        TextField defermentStartDateField = new TextField();
        TextField defermentEndDateField = new TextField();
        Text defermentStartDateError = new Text();
        Text defermentEndDateError = new Text();
        defermentStartDateError.setFill(Color.RED);
        defermentEndDateError.setFill(Color.RED);

        if(this.defermentStartDate != null)
        {
            defermentStartDateField.setText(this.defermentStartDate.toString());
            defermentEndDateField.setText(this.defermentEndDate.toString());
        }

        Button resetDefermentBtn = new Button("Išimti mokėjimų atostogas");
        Button setDefermentBtn = new Button("Nustatyti mokėjimų atostogas");

        resetDefermentBtn.setOnAction(
                evt -> this.resetDeferment()
        );

        setDefermentBtn.setOnAction(
                evt -> this.setDeferment(
                        defermentStartDateField,
                        defermentEndDateField,
                        defermentStartDateError,
                        defermentEndDateError
                )
        );

        return new VBox(10,
                new VBox(
                        10,
                        new VBox(
                                defermentStartDateLabel,
                                defermentStartDateField,
                                defermentStartDateError
                        ),
                        new VBox(
                                defermentEndDateLabel,
                                defermentEndDateField,
                                defermentEndDateError
                        )
                ),
                new VBox(
                        10,
                        setDefermentBtn,
                        resetDefermentBtn
                )
        );
    }

    private void stopDefermentPopup() {
        if (this.defermentPopup == null)
            return;

        this.defermentPopup.close();
        this.defermentPopup = null;
    }

    private void updateDefermentText()
    {
        if(this.defermentStartDate == null)
            deferementPeriodText.setText("Mokėjimų atostogos: Nėra");

        else
            deferementPeriodText.setText("Mokėjimų atostogos: nuo " + this.defermentStartDate.toString() + " iki " + this.defermentEndDate.toString());
    }

    public Loan getLoan() {
        return this.loan;
    }
}
