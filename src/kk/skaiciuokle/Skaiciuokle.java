package kk.skaiciuokle;

import javafx.application.Application;
import javafx.stage.Stage;
import kk.skaiciuokle.views.index.IndexView;
import kk.skaiciuokle.views.View;

public class Skaiciuokle extends Application {
    private Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        this.setUpStage(stage);
        this.setActiveView(new IndexView(this));
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    public void setActiveView(View view) {
        this.stage.setScene(view.buildScene());
    }

    private void setUpStage(Stage stage) {
        this.stage = stage;

        stage.setTitle("Būsto paskolos skaičiuoklė | Karolis Kraujelis");
        stage.setWidth(1280);
        stage.setHeight(1000);
        stage.setResizable(false);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
